package com.aldes.jcs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import com.trolltech.qt.QThread;

/**
 * Download Task
 *
 * @author JenChieh
 */
public class DownloadTask implements Runnable {

    private QThread downloadProcess = null;

    public void start() {
        // this thread can only be start once.
        if (downloadProcess != null)
            return;

        downloadProcess = new QThread(this);
        downloadProcess.start();
    }

    @Override
    public void run() {
        
        // count how many programs is completed?
        int count = 0;
        
        for (DownloadItem di : ProgramMain.DOWNLOAD_ITEMS) {
            count++;
            
            if (di.isCheck) {
                // download the file.
                try {
                    ProgramMain.STATUS_DESC = "Downloading " + di.getProgramName() + "...";
                    
                    downloadFile(di.getItemURL(), di.getProgramName());
                    
                    ProgramMain.PROGRESS_VALUE = count / ProgramMain.DOWNLOAD_ITEMS.size() * 100;

                    ProgramMain.STATUS_DESC = "Running " + di.getProgramName() + "...";
                    
                    // execute the program.
                    runExe(di.getProgramName());

                    // TODO(jenchieh): remove the downloaded files?
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // done processing.
        ProgramMain.STATUS_DESC = "Done.";
    }

    /**
     *
     * @param url
     * @throws IOException
     */
    private void downloadFile(String url, String programName)
        throws IOException {
        URL website = new URL(url);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(
                Downloader.DOWNLOAD_PATH + programName);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
    }

    /**
     * Run a executable.
     *
     * @throws IOException
     */
    private void runExe(String programName) throws IOException {
        // Remember enable administrator mode.
        Process process = new ProcessBuilder(
                Downloader.DOWNLOAD_PATH + programName).start();
        
        while (true) { 
            if (!process.isAlive())
                break;
        }
    }

}
