package com.aldes.jcs;

import java.util.ArrayList;
import java.util.Properties;

import com.aldes.jcsqtj.core.JCSQtJ_Window;
import com.trolltech.qt.core.QEvent;
import com.trolltech.qt.core.QTimer;
import com.trolltech.qt.gui.QCheckBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QLineEdit;
import com.trolltech.qt.gui.QProgressBar;
import com.trolltech.qt.gui.QPushButton;
import com.trolltech.qt.gui.QTextEdit;

/**
 * $File: Main.java $
 * $Date: 2018-04-05 22:11:07 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2018 by Shen, Jen-Chieh $
 */

/**
 * Program Entry Class. (Start)
 *
 * @author JenChieh
 */
public class ProgramMain {

    public static JCSQtJ_Window MAIN_WINDOW = null;

    public static String WINDOW_TITLE = "One Click Install";
    public static String DATA_PATH = "data/";
    public final static String DOWNLOAD_LIST = "list.ini";
    public final static String DOWNLOAD_INI_PATH = DATA_PATH + DOWNLOAD_LIST;

    public static int WINDOW_WIDTH = 800;
    public static int WINDOW_HEIGHT = 600;

    public final static int GUI_UPDATE_RATE =  100;

    private static String ICON_IMAGE_PATH = DATA_PATH + "icon.ico";

    private final static INI_Parser DOWNLOAD_PROPS = new INI_Parser();
    public static ArrayList<DownloadItem> DOWNLOAD_ITEMS = new ArrayList<DownloadItem>();

    private static int startingCBPosX = 70;
    private static int startingRBPosX = 10;

    private static int startingNameLabelPosX = 100;
    private static int startingLinkLabelPosX = 200;

    private static int startingItmeListY = 60;

    // Current how many download item on the list?
    public static int CURRENT_ITEM_COUNT = 0;

    private static int ITEM_INTERVAL = 20;

    public static QLineEdit LE_name = null;
    public static QLineEdit LE_url = null;

    public static QProgressBar PROGRESS_BAR = null;
    public static int PROGRESS_VALUE = 0;

    public static QPushButton START_DOWNLOAD_BUTTON = null;

    public final static String DEFAULT_STATUS_DESC = "";
    public static QTextEdit STATUS_BAR = null;
    public static String STATUS_DESC = DEFAULT_STATUS_DESC;  // status description.

    // Core methods will all from this class, `Downloader' class.
    public static Downloader downloader = new Downloader(DOWNLOAD_ITEMS);


    /**
     * Program Entry.
     *
     * @param args : arguments array.
     */
    public final static void main(String[] args) {
        MAIN_WINDOW = new JCSQtJ_Window();
        MAIN_WINDOW.startFrame(args);

        MAIN_WINDOW.setWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        MAIN_WINDOW.setWindowTitle(WINDOW_TITLE);
        MAIN_WINDOW.setIconImage(ICON_IMAGE_PATH);
        // ----------------------------------------------

        int addRowPosY = 15;

        QLabel LB_name = MAIN_WINDOW.addLabelToWindow("Name: ", 15, addRowPosY);
        QLabel LB_url = MAIN_WINDOW.addLabelToWindow("URL: ", 170, addRowPosY);

        LE_name = MAIN_WINDOW.addLineEditToWindow(55, addRowPosY);
        LE_url = MAIN_WINDOW.addLineEditToWindow(200, addRowPosY, 475, 30);

        QPushButton addNewItemBtn = MAIN_WINDOW.addButtonToWindow(
            "Add Item",
            690,
            addRowPosY,
            downloader,
            "addNewItemButton()");

        showDownloadList();

        /* Progress Bar */
        PROGRESS_BAR = MAIN_WINDOW.addProgressBarToWindow(525, WINDOW_HEIGHT - 40, 150, 30);
        PROGRESS_BAR.setValue(0);  // starts from 0.

        /* Text Edit */
        STATUS_BAR = MAIN_WINDOW.addTextEditToWindow(
            10,
            WINDOW_HEIGHT - 40,
            500,
            30);
        STATUS_BAR.setEnabled(false);
        STATUS_BAR.setText(DEFAULT_STATUS_DESC);

        /* Button */
        START_DOWNLOAD_BUTTON = MAIN_WINDOW.addButtonToWindow(
            "Start Download",
            0, 0,  // these will get reset bellow.
            downloader,
            "startDownloadBtn()");
        // reset x and y position again cuz of the un-initialize
        // complete issue.
        START_DOWNLOAD_BUTTON.move(
            690,
            WINDOW_HEIGHT - 40);

        /* Regiester Events */
        QTimer timer = new QTimer();
        timer.timeout.connect(downloader, "timerUpdate()");
        timer.start(GUI_UPDATE_RATE);

        // ----------------------------------------------
        MAIN_WINDOW.endFrame();
    }

    /**
     * Read the .ini file and update GUI.
     */
    public static void showDownloadList() {

        // reread the .ini file.
        DOWNLOAD_PROPS.ReadINIFile(DATA_PATH + DOWNLOAD_LIST);

        // get properties from ini_parser class.
        Properties props = DOWNLOAD_PROPS.getProperties();

        // No properties? do nothing then.
        if (props.size() == 0)
            return;

        for (Object key : props.keySet()) {

            // First transfer the key value to string.
            String keyName = key.toString();
            String itemURL = DOWNLOAD_PROPS.getProperty(keyName);

            addOneDownloadItem(keyName, itemURL);

            // add up the currnet item count.
            ++CURRENT_ITEM_COUNT;
        }
    }

    /**
     * Return current item next added position on Y axis.
     * @param key : key is also the item/software name here.
     * @param itemURL : item's correspondingg URL.
     */
    public static void addOneDownloadItem(String keyName, String itemURL) {

        DownloadItem dwnItem = new DownloadItem();

        dwnItem.setItemName(keyName);
        dwnItem.setItemURL(itemURL);

        /* Add delete buttons */
        {
            QPushButton removeBtn = MAIN_WINDOW.addButtonToWindow(
                "Remove",
                startingRBPosX,
                currentItemPosY()+ 5,
                50,
                15,
                dwnItem,
                "removeItem()");

            dwnItem.setRemoveBtn(removeBtn);
        }

        /* Add combo box. */
        {
            QCheckBox tmpCheckBox = MAIN_WINDOW.addCheckBoxToWindow(
                startingCBPosX,
                currentItemPosY());

            // Is check as default settings.
            tmpCheckBox.setChecked(true);

            dwnItem.setCheckBox(tmpCheckBox);
        }

        /* Add Name Label */
        {
            QLabel nameLabel = MAIN_WINDOW.addLabelToWindow(
                keyName,
                startingNameLabelPosX,
                currentItemPosY());

            dwnItem.setNameLabel(nameLabel);
        }

        /* Add Link Label */
        {
            QLabel linkLabel = MAIN_WINDOW.addLabelToWindow(
                itemURL,
                startingLinkLabelPosX,
                currentItemPosY(),
                1000, 30);

            dwnItem.setLinkLabel(linkLabel);
        }

        // collect all download list and it's status.
        DOWNLOAD_ITEMS.add(dwnItem);
    }

    /**
     * Return current item next added position on Y axis.
     *
     * @return value of current item position on Y axis.
     */
    public static final int currentItemPosY() {
        return startingItmeListY + (ITEM_INTERVAL * CURRENT_ITEM_COUNT);
    }
}
