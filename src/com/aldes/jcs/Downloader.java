package com.aldes.jcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.trolltech.qt.gui.QVBoxLayout;

/**
 * $File: Downloader.java $
 * $Date: 2018-04-07 23:03:35 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2018 by Shen, Jen-Chieh $
 */


/**
 * Main class for downloading file and manage everything
 * related to download.
 *
 * NOTE(jenchieh): currently handle running executable too.
 */
public class Downloader {

    public final static String DOWNLOAD_PATH = "./download/";

    private DownloadTask downlodTask = null;

    private ArrayList<DownloadItem> downloads = new ArrayList<DownloadItem>();

    public Downloader(ArrayList<DownloadItem> dwnItem) {
        this.downloads = dwnItem;
    }

    /**
     * Add the new item button.
     * @throws IOException
     */
    public void addNewItemButton() throws IOException {

        String newName = ProgramMain.LE_name.text();
        String newUrl = ProgramMain.LE_url.text();

        if (newName.equals("") || newUrl.equals(""))
            return;

        BufferedWriter fw = new BufferedWriter(new FileWriter(ProgramMain.DOWNLOAD_INI_PATH, true));
        fw.write(newName + "=" + newUrl + "\n");
        fw.close();

        // Clear the both line edit area.
        ProgramMain.LE_name.clear();
        ProgramMain.LE_url.clear();

        // TODO(jenchieh): Jambi have lack of documentation, dynamic
        // GUI change seems impossible to find an proper example 
        // from Google or Baidu. I do not think I am able to add this
        // feature myself without Qt Jambi developers..
        // 
        // Add one download item.
        ProgramMain.addOneDownloadItem(newName, newUrl);
    }

    /**
     * Start download all checked the softwares.
     * @throws IOException
     */
    public void startDownloadBtn() throws IOException {

        for (DownloadItem di : ProgramMain.DOWNLOAD_ITEMS) {
            di.isCheck = di.getCheckBox().isChecked();
        }

        // can only have one download task.
        if (downlodTask == null) {
            // Start the thread.
            downlodTask = new DownloadTask();
            downlodTask.start();
        }

        // TODO(jenchieh): we need to enable this back to true!
        ProgramMain.START_DOWNLOAD_BUTTON.setEnabled(false);
    }

    /**
     * Remove a property from file.
     * @param prop
     * @throws IOException
     */
    public static void removePropertyFromFile(final String filePath, final String prop) throws IOException {
        File inputFile = new File(filePath);
        File tempFile = new File(filePath + ".tmp");

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String currentLine = "";

        while((currentLine = reader.readLine()) != null) {
            if (currentLine.contains(prop))
                continue;
            /*
             * NOTE(jenchieh): line.separator will return either
             *   - \r
             *   - \r\n
             *   - \n
             * depends on the OS.
             */
            writer.write(currentLine + System.getProperty("line.separator"));
        }

        writer.close();
        reader.close();

        // delete the old file.
        inputFile.delete();

        // rename the file.
        boolean successful = tempFile.renameTo(inputFile);
    }

    /**
     * Run every milliseconds.
     */
    private void timerUpdate() {
        ProgramMain.PROGRESS_BAR.setValue(ProgramMain.PROGRESS_VALUE);
        ProgramMain.STATUS_BAR.setText(ProgramMain.STATUS_DESC);
    }
}
