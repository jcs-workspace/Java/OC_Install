package com.aldes.jcs;

import java.io.IOException;

import com.trolltech.qt.gui.QCheckBox;
import com.trolltech.qt.gui.QLabel;
import com.trolltech.qt.gui.QPushButton;

/**
 * $File: DownloadItem.java $
 * $Date: 2018-04-07 23:18:25 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2018 by Shen, Jen-Chieh $
 */


public class DownloadItem {

    private QCheckBox checkBox = null;
    private QLabel nameLabel = null; 
    private QLabel linkLabel = null;
    private QPushButton removeBtn = null;
    
    private String itemName = "";  // property name.
    private String itemURL = "";  // property value.
    
    // According to property value, get the program name.
    // Usually the last part of the url.
    private String programName = "";
    
    public boolean isCheck = true;
    

    public DownloadItem() {
        
    }
    
    /**
     * Remove a properties from .properties file.
     * @throws IOException 
     */
    private void removeItem() throws IOException {
        Downloader.removePropertyFromFile(
                ProgramMain.DOWNLOAD_INI_PATH, 
                getItemName());
        
        // make invisible to all GUI.
        this.getNameLabel().setVisible(false);
        this.getLinkLabel().setVisible(false);
        this.getCheckBox().setVisible(false);
        this.getRemoveBtn().setVisible(false);
    }
    
    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
    
    public void setCheckBox(QCheckBox checkBox) {
        this.checkBox = checkBox;
    }
    
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    
    public void setItemURL(String itemURL) {
        this.itemURL = itemURL;
        
        // also get the program name if we get the url.
        if (itemURL.contains("/"))
            setProgramName(itemURL.substring(itemURL.lastIndexOf("/")));
    }
    
    public QCheckBox getCheckBox() {
        return checkBox;
    }
    
    public String getItemName() {
        return itemName;
    }
    
    public String getItemURL() {
        return itemURL;
    }
    
    public void setLinkLabel(QLabel label) {
        this.linkLabel = label;
    }
    
    public void setNameLabel(QLabel label) {
        this.nameLabel = label;
    }
    
    public QLabel getLinkLabel() {
        return this.linkLabel;
    }
    
    public QLabel getNameLabel () {
        return this.nameLabel;
    }
    
    public void setRemoveBtn(QPushButton removeBtn) {
        this.removeBtn = removeBtn;
    }
    
    public QPushButton getRemoveBtn() {
        return this.removeBtn;
    }
}
