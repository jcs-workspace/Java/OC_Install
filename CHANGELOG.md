# Change Log

All notable changes to this project will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.


## 2018-04-10

* Stop the project because Qt Jambi no longer alive. (OC_Install)

## 2018-04-09

* Update progress bar and status bar by using the QTimer class which is the timer event API that Qt framework provided. (OC_Install)

## 2018-04-08

* Able to download the software targeting. (OC_Install)
* Able to run the software that are just downloaded. (OC_Install)

## 2018-04-05

* First setup project. (OC_Install)
